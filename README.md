Proyecto de Mario Kart 3D creado por Willi e Ismael

Controles del Personaje:
- Movimiento con A y D para moverte de de izquierda a derecha
- Movimiento con S para la marcha atras
- Barra del espacio para acelerar
- La tecla V para los derrapes(Se tiene que mantener para lograr los efectos)
- Tambien se puede mover con las flechas del teclado(Esto se usa mas para cuando estas planeado o volando)


Requisitos minimos:
- Juego en 3D
- Camara en 3D
- Faros de iluminacion en el coche que alumbran por donde pasa
- Movimiento en las 3 dimensiones(Fisicas 3D y rotacion)
- Turbo del personaje al derrapar
- Cuestas y saltos
- Raycast

Requisitos adiccionales:
- Materiales 3D
- Contador de vueltas 
- Musica
- Contador de monedas y monedas que puedes recolectar por el circuito
- Al caer vuelves al inicio 
- Finish al completar todas las vueltas marcadas
- Habia vuelo de Cometa pero se quedo bugg por algun motivo y no se pudo enseñar
- No se termino de poner las cajas misteriosas
